package uk.ac.kcl.teamredwood.infographic.database;

import android.net.Uri;
import android.test.AndroidTestCase;

public class InfographicContractTest extends AndroidTestCase {

    public void testBuildDataUri() {
        final int ID_TEST = 1;
        Uri dataUri = InfographicContract.WordlBankDataEntry.buidDataUri(ID_TEST);
        assertNotNull("Null Uri returned", dataUri);
        assertEquals("Problem with appending Id to the Uri",
                Integer.toString(ID_TEST), dataUri.getLastPathSegment());
        assertEquals("Uri does not match expected result",
                dataUri.toString(), "content://uk.ac.kcl.teamredwood.infographic/world_bank/1");
    }
}
