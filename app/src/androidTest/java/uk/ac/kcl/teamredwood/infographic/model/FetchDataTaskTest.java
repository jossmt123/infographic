package uk.ac.kcl.teamredwood.infographic.model;

import android.test.AndroidTestCase;

import uk.ac.kcl.teamredwood.infographic.MainActivity;

public class FetchDataTaskTest extends AndroidTestCase {

    public void testAsyncTask() {
        if(!MainActivity.isNetworkAvailable(mContext)) {
            fail("There is no network connection. Could not test the class.");
        }
        FetchDataTask asyncTask = new FetchDataTask(mContext);
        String jsonResponse = asyncTask.doInBackground("FRA", "SL.TLF.PART.ZS");
        assertTrue("Connection was successful and json has been fetched",
                jsonResponse != null && !jsonResponse.isEmpty());
    }
}
