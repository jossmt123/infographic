package uk.ac.kcl.teamredwood.infographic.database;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.test.AndroidTestCase;

import java.util.HashSet;

public class InfographicDbHelperTest extends AndroidTestCase {

    public void setUp() {
        deleteWorldBankDb();
    }

    public void deleteWorldBankDb() {
        mContext.deleteDatabase(InfographicDbHelper.DATABASE_NAME);
    }

    public void testCreateDb() throws Throwable {

        final HashSet<String> tableNameHashSet = new HashSet<>();
        tableNameHashSet.add(InfographicContract.WordlBankDataEntry.TABLE_NAME);

        SQLiteDatabase db = new InfographicDbHelper(this.mContext).getWritableDatabase();
        assertEquals(true, db.isOpen());

        Cursor c = db.rawQuery("SELECT name FROM sqlite_master WHERE type='table'", null);

        assertTrue("The database has not been created correctly", c.moveToFirst());

        do {
            tableNameHashSet.remove(c.getString(0));
        } while(c.moveToNext());

        assertTrue("The database has been created without required tables", tableNameHashSet.isEmpty());

        // Test the world_bank table
        c = db.rawQuery("PRAGMA table_info(" + InfographicContract.WordlBankDataEntry.TABLE_NAME + ")", null);

        assertTrue("Unable to query the db", c.moveToFirst());

        final HashSet<String> worldBankColumnsHashSet = new HashSet<>();
        worldBankColumnsHashSet.add(InfographicContract.WordlBankDataEntry._ID);
        worldBankColumnsHashSet.add(InfographicContract.WordlBankDataEntry.COLUMN_INDICATOR);
        worldBankColumnsHashSet.add(InfographicContract.WordlBankDataEntry.COLUMN_DATE);
        worldBankColumnsHashSet.add(InfographicContract.WordlBankDataEntry.COLUMN_VALUE);

        int columnNameIndex = c.getColumnIndex("name");
        do {
            String columnName = c.getString(columnNameIndex);
            worldBankColumnsHashSet.remove(columnName);
        } while(c.moveToNext());

        assertTrue("The database does not contain required WorldBankDataEntry columns", worldBankColumnsHashSet.isEmpty());

        db.close();
    }

    public void testWorldBankDataTable() {
        InfographicDbHelper dbHelper = new InfographicDbHelper(mContext);
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(InfographicContract.WordlBankDataEntry.COLUMN_INDICATOR, "KCL/INDICATOR");
        values.put(InfographicContract.WordlBankDataEntry.COLUMN_VALUE, 15);
        values.put(InfographicContract.WordlBankDataEntry.COLUMN_DATE, 2015);

        long worldBankRowId = db.insert(InfographicContract.WordlBankDataEntry.TABLE_NAME, null, values);
        assertTrue(worldBankRowId != -1);

        Cursor worldBankCursor = db.query(
                InfographicContract.WordlBankDataEntry.TABLE_NAME,
                null, null, null, null, null, null
        );

        assertTrue("No records returned from the WorldBank table", worldBankCursor.moveToFirst());

        assertFalse("More than one record returned from the WorldBank table", worldBankCursor.moveToNext());

        worldBankCursor.close();
        dbHelper.close();
    }
}
