package uk.ac.kcl.teamredwood.infographic.university_rank;

import android.test.AndroidTestCase;

import java.util.ArrayList;

public class QsRankModelTest extends AndroidTestCase {

    public void testReadCsvAndRankModel() {
        QsRankModel qsRankModel = new QsRankModel(mContext);

        assertTrue("Global rank has been read", !qsRankModel.getUniversityRankData().isEmpty());
        assertTrue("Global rank contains more than hundred entries",
                qsRankModel.getUniversityRankData().size() > 100);

        assertTrue("Cities rank has been read", !qsRankModel.getCitiesRankData().isEmpty());
        assertTrue("Cities rank contains more than fifty entries",
                qsRankModel.getCitiesRankData().size() > 50);
    }

    public void testSelectCountrySpecificData() {
        final String TESTED_COUNTRY = "France";
        QsRankModel qsRankModel = new QsRankModel(mContext);
        ArrayList<String[]> listRankModel = qsRankModel.getArrayLocalRankModel(TESTED_COUNTRY);

        assertTrue("Local rank has been created", !listRankModel.isEmpty());
        assertTrue("Local rank contains more than five entries",
                listRankModel.size() > 5);
    }
}
