package uk.ac.kcl.teamredwood.infographic;

import android.graphics.drawable.Drawable;
import android.test.ActivityInstrumentationTestCase2;
import android.widget.ImageView;
import android.widget.TextView;

import org.junit.Test;

public class PTEmploymentPanelTest extends ActivityInstrumentationTestCase2<MainActivity> {

    public PTEmploymentPanelTest() {
        super(MainActivity.class);
    }

    @Test
    public void testActivityExists() {
        assertNotNull(MainActivity.class);
    }


    @Test
    public void textFieldTest(){
        MainActivity designActivity = getActivity();

        final TextView text = (TextView)designActivity.findViewById(R.id.percentage);
        int percentage = designActivity.percentage;

        getInstrumentation().runOnMainSync(new Runnable() {
            @Override
            public void run() {
                text.requestFocus();
            }
        });

        String currentText = (String)text.getText();
        assertEquals(currentText, percentage + "%");
    }
    @Test
    public void imageViewTest(){
        MainActivity designActivity = getActivity();

        final ImageView image = (ImageView)designActivity.findViewById(R.id.imageView);
        int percentage = designActivity.percentage;

        getInstrumentation().runOnMainSync(new Runnable() {
            @Override
            public void run() {
                image.requestFocus();
            }
        });

        Drawable imagex = image.getDrawable();
        if(percentage<=10){
            assertEquals(imagex, R.drawable.employment10);
        }else if(percentage>10 && percentage<=15){
            assertEquals(imagex, R.drawable.employment15);
        }else if(percentage>15 && percentage<=20){
            assertEquals(imagex, R.drawable.employment20);
        }else if(percentage>20 && percentage<=25) {
            assertEquals(imagex, R.drawable.employment25);
        }else if(percentage>25 && percentage<=30) {
            assertEquals(imagex, R.drawable.employment30);
        }else if(percentage>30 && percentage<=35) {
            assertEquals(imagex, R.drawable.employment35);
        }else if(percentage>35) {
            assertEquals(imagex, R.drawable.employment40);
        }
    }
}
