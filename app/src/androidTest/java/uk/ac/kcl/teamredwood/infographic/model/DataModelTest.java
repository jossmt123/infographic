package uk.ac.kcl.teamredwood.infographic.model;

import android.test.AndroidTestCase;
import android.util.Log;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.HashMap;

import uk.ac.kcl.teamredwood.infographic.MainActivity;

public class DataModelTest extends AndroidTestCase {

    final private String TESTED_COUNTRY = "FRA";
    final private String TESTED_INDICATOR = "SL.TLF.PART.ZS";

    private String mJson;
    private ArrayList<DataModel> listDataModel;

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        mJson = getJson();
    }

    private String getJson() {
        if(!MainActivity.isNetworkAvailable(mContext)) {
            fail("There is no network connection. Could not test the class.");
        }
        FetchDataTask asyncTask = new FetchDataTask(mContext);
        String jsonResponse = asyncTask.doInBackground(TESTED_COUNTRY, TESTED_INDICATOR);
        return jsonResponse;
    }

    public void testParseJson() throws JSONException {
        listDataModel = DataModel.parseJSON(mJson, TESTED_COUNTRY + "/" + TESTED_INDICATOR);
        Log.d("ds", mJson);
        assertTrue("Json has been parsed successfully", listDataModel != null && !listDataModel.isEmpty());

        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("INDICATOR", TESTED_COUNTRY + "/" + TESTED_INDICATOR);

        DataModel testDataModel = new DataModel(hashMap);
        DataModel dataModel = listDataModel.get(0);

        assertEquals("Data models created", testDataModel.getIndicatorKey(), dataModel.getIndicatorKey());
    }
}
