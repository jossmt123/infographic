package uk.ac.kcl.teamredwood.infographic;

import android.app.AlertDialog;
import android.test.ActivityInstrumentationTestCase2;
import android.test.TouchUtils;
import android.widget.Button;

public class CountryDialogFragmentTest extends ActivityInstrumentationTestCase2<MainActivity> {

    public CountryDialogFragmentTest() {
        super(MainActivity.class);
    }

    public void testDialogShowsAndCountrySelection() throws InterruptedException {
        MainActivity mainActivity = getActivity();

        android.support.v7.view.menu.ActionMenuItemView menuItemCountry =
                (android.support.v7.view.menu.ActionMenuItemView) mainActivity.findViewById(R.id.menu_country_btn);

        TouchUtils.clickView(this, menuItemCountry);
        Thread.sleep(3000);

        CountryDialogFragment countryDialogFragment = (CountryDialogFragment)
                mainActivity.getFragmentManager().findFragmentByTag(CountryDialogFragment.class.getSimpleName());
        AlertDialog alertDialog = (AlertDialog) countryDialogFragment.getDialog();

        assertTrue("The Select Country dialog has been displayed", alertDialog != null);

        Button btnFrance = (Button) alertDialog.findViewById(R.id.btnFrance);

        TouchUtils.clickView(this, btnFrance);
        Thread.sleep(3000);

        String selectedCountry = mainActivity.selectedCountry;
        final String TEST_COUNTRY = "France";

        assertEquals("The country has been selected", TEST_COUNTRY, selectedCountry);
    }
}
