package uk.ac.kcl.teamredwood.infographic.database;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.test.AndroidTestCase;

public class DataProviderTest extends AndroidTestCase {

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        clearData();
    }

    public void clearData() {
        mContext.getContentResolver().delete(
                InfographicContract.WordlBankDataEntry.CONTENT_URI,
                null,
                null
        );

        Cursor cursor = mContext.getContentResolver().query(
                InfographicContract.WordlBankDataEntry.CONTENT_URI,
                null,
                null,
                null,
                null
        );
        assertEquals("Data has not been removed from the WorldBank table", 0, cursor.getCount());
        cursor.close();
    }

    public void testWorldBankQuery() {
        InfographicDbHelper dbHelper = new InfographicDbHelper(mContext);
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(InfographicContract.WordlBankDataEntry.COLUMN_INDICATOR, "COUNTRY/TEST.INDICATOR");
        values.put(InfographicContract.WordlBankDataEntry.COLUMN_DATE, "2015");
        values.put(InfographicContract.WordlBankDataEntry.COLUMN_VALUE, "100");

        long worldBankRowId = db.insert(InfographicContract.WordlBankDataEntry.TABLE_NAME, null, values);
        assertTrue(worldBankRowId != -1);

        db.close();

        Cursor dataCursor = mContext.getContentResolver().query(
                InfographicContract.WordlBankDataEntry.CONTENT_URI,
                null,
                null,
                null,
                null
        );

        assertTrue("No records returned from the WorldBank table", dataCursor.moveToFirst());

        assertFalse("More than one record returned from the WorldBank table", dataCursor.moveToNext());

        dataCursor.close();
    }
}
