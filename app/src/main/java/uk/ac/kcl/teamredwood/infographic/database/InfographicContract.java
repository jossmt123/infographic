package uk.ac.kcl.teamredwood.infographic.database;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.net.Uri;
import android.provider.BaseColumns;

/**
 * Defines table and column names for the infographic database.
 */
public class InfographicContract {

    // Name of the content provider
    public static final String CONTENT_AUTHORITY = "uk.ac.kcl.teamredwood.infographic";

    // The base of a URI for contacting the content provider
    public static final Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY);

    // Paths for looking for worlbank data
    public static final String PATH_WORLD_BANK_DATA = "world_bank";

    /* Inner class that defines the table contents of the wordlbank table */
    public static final class WordlBankDataEntry implements BaseColumns {

        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_WORLD_BANK_DATA).build();

        public static final String CONTENT_TYPE =
                ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_WORLD_BANK_DATA;

        // Table name
        public static final String TABLE_NAME = "world_bank";
        // String in the form of "country/indicator", e.g. FRA/14.1_AGR.ENERGY.INTENSITY
        public static final String COLUMN_INDICATOR = "indicator";
        // Year for which data has been collected
        public static final String COLUMN_DATE = "date";
        // Value corresponding to a date
        public static final String COLUMN_VALUE = "value";

        public static Uri buidDataUri(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }

    }


}