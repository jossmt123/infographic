package uk.ac.kcl.teamredwood.infographic.database;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.annotation.Nullable;

/**
 * A content provider manages access to a central repository of data
 * and provides the additional abstraction layer.
 */
public class DataProvider extends ContentProvider {

    private static final UriMatcher sUriMatcher = buildUriMatcher();

    private InfographicDbHelper mDbHelper;

    static final int WORLD_BANK = 100;

    @Override
    public boolean onCreate() {
        mDbHelper = new InfographicDbHelper(getContext());
        return true;
    }

    // Retrieve data from a provider
    @Nullable
    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        Cursor returnCursor;

        switch (sUriMatcher.match(uri)) {
            case WORLD_BANK: {
                returnCursor = mDbHelper.getReadableDatabase().query(
                        InfographicContract.WordlBankDataEntry.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder
                );
                break;
            }
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }

        returnCursor.setNotificationUri(getContext().getContentResolver(), uri);
        return returnCursor;
    }

    // Return the MIME type corresponding to a content URI
    @Nullable
    @Override
    public String getType(Uri uri) {
        final int match = sUriMatcher.match(uri);

        switch (match) {
            case WORLD_BANK:
                return InfographicContract.WordlBankDataEntry.CONTENT_TYPE;
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
    }

    // Insert a row into a provider
    @Nullable
    @Override
    public Uri insert(Uri uri, ContentValues values) {
        final SQLiteDatabase db = mDbHelper.getWritableDatabase();
        final int match = sUriMatcher.match(uri);
        Uri returnUri;

        switch (match) {
            case WORLD_BANK: {
                long _id = db.insert(InfographicContract.WordlBankDataEntry.TABLE_NAME, null, values);
                if (_id > 0)
                    returnUri = InfographicContract.WordlBankDataEntry.buidDataUri(_id);
                else
                    throw new android.database.SQLException("Failed to insert row into " + uri);
                break;
            }
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }

        getContext().getContentResolver().notifyChange(uri, null);
        return returnUri;
    }

    // Delete rows from a provider
    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        final SQLiteDatabase db = mDbHelper.getWritableDatabase();
        final int match = sUriMatcher.match(uri);
        int rowsDeleted;

        // a null deletes all rows
        if (selection == null) selection = "1";

        switch (match) {
            case WORLD_BANK: {
                rowsDeleted = db.delete(
                        InfographicContract.WordlBankDataEntry.TABLE_NAME, selection, selectionArgs);
                break;
            }
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }

        if (rowsDeleted != 0) {
            getContext().getContentResolver().notifyChange(uri, null);
        }
        return rowsDeleted;
    }

    // Update existing rows in a provider
    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        final SQLiteDatabase db = mDbHelper.getWritableDatabase();
        final int match = sUriMatcher.match(uri);
        int rowsUpdated;

        switch (match) {
            case WORLD_BANK: {
                rowsUpdated = db.update(
                        InfographicContract.WordlBankDataEntry.TABLE_NAME, values, selection, selectionArgs);
                break;
            }
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }

        return rowsUpdated;
    }

    // The UriMatcher matches each URI to the WORLD_BANK
    static UriMatcher buildUriMatcher() {
        final UriMatcher matcher = new UriMatcher(UriMatcher.NO_MATCH);
        final String authority = InfographicContract.CONTENT_AUTHORITY;

        matcher.addURI(authority, InfographicContract.PATH_WORLD_BANK_DATA, WORLD_BANK);
        return matcher;
    }
}
