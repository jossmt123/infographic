package uk.ac.kcl.teamredwood.infographic.model;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import uk.ac.kcl.teamredwood.infographic.MainActivity;
import uk.ac.kcl.teamredwood.infographic.R;

public class FetchDataTask extends AsyncTask<String, Void, String> {

    public final static String LOG_TAG = FetchDataTask.class.getSimpleName();

    private String mapIndicator;

    private Context context;

    public interface CallbackDataTask {
        void onTaskFinished(String jsonResponse, String mapIndicator);
        void onAllTasksFinished();
        boolean allTasksFinished();
    }

    public FetchDataTask(Context context) {
        this.context = context;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected String doInBackground(String... params) {
        HttpURLConnection httpURLConnection = null;
        BufferedReader bufferedReader = null;
        String jsonResponse = null;

        try {
            String country = params[0];
            String indicator = params[1];

            mapIndicator = country + "/" + indicator;

            final String path = context.getString(R.string.address_full, country, indicator);

            URL finalURL = new URL(path);
            Log.d(LOG_TAG, finalURL.toString());

            httpURLConnection = (HttpURLConnection) finalURL.openConnection();
            httpURLConnection.setRequestMethod("GET");
            try {
                httpURLConnection.connect();
            } catch (Exception e) {
                Log.e(LOG_TAG, "httpURLConnection error");
                return null;
            }

            InputStream inputStream = httpURLConnection.getInputStream();
            StringBuffer buffer = new StringBuffer();

            if (inputStream == null) {
                return null;
            }

            bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

            String jsonText;

            while ((jsonText = bufferedReader.readLine()) != null) {
                buffer.append(jsonText);
            }

            if (buffer.length() == 0) {
                return null;
            }

            jsonResponse = buffer.toString();
        } catch (IOException ex) {
            Log.e(LOG_TAG, ex.getMessage());
        } finally {
            if (httpURLConnection != null) {
                httpURLConnection.disconnect();
            }
            if (bufferedReader != null) {
                try {
                    bufferedReader.close();
                } catch (IOException ex) {
                    Log.e(LOG_TAG, ex.getMessage());
                }
            }
        }

        return jsonResponse;
    }

    @Override
    protected void onPostExecute(String jsonResponse) {
        super.onPostExecute(jsonResponse);
        ((MainActivity) context).onTaskFinished(jsonResponse, mapIndicator);
    }
}