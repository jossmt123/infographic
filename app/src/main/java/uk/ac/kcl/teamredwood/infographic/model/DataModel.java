package uk.ac.kcl.teamredwood.infographic.model;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import uk.ac.kcl.teamredwood.infographic.database.InfographicContract;

public class DataModel {

    private final static String VALUE_KEY = "VALUE";
    private final static String DATE_KEY = "DATE";
    private final static String INDICATOR_KEY = "INDICATOR";

    private String value, date, mapIndicator;

    public DataModel(HashMap<String, String> hashMap) {
        value = hashMap.get(VALUE_KEY);
        date = hashMap.get(DATE_KEY);
        mapIndicator = hashMap.get(INDICATOR_KEY);
    }

    public String getValue() { return value; }

    public String getDate() {
        return date;
    }

    public String getIndicatorKey() { return mapIndicator; }

    public static ArrayList<DataModel> parseJSON(String jsonResponse, String mapIndicator) throws JSONException {
        JSONArray jsonArray = new JSONArray(jsonResponse).getJSONArray(1);

        HashMap<String, String> hashMapData = new HashMap<>();
        ArrayList<DataModel> listDataModel = new ArrayList<>();

        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject dataObject = jsonArray.getJSONObject(i);
            String value = dataObject.getString("value");
            String date = dataObject.getString("date");

            if(value != null && date != null && !value.isEmpty() && !date.isEmpty() &&
                    !value.equals("null") && !date.equals("null")) {

                hashMapData.put(VALUE_KEY, value);
                hashMapData.put(DATE_KEY, date);
                hashMapData.put(INDICATOR_KEY, mapIndicator);

                listDataModel.add(new DataModel(hashMapData));
            }
        }
        return listDataModel;
    }

    public static ArrayList<DataModel> retrieveData(String dbIndicator, Context context) {

        String selectionClause = InfographicContract.WordlBankDataEntry.COLUMN_INDICATOR + " = ?";

        Cursor cursor = context.getContentResolver().query(
                InfographicContract.WordlBankDataEntry.CONTENT_URI,
                null,
                selectionClause,
                new String[]{dbIndicator},
                null);

        ArrayList<DataModel> listDataModel = new ArrayList<>();

        if (cursor != null) {
            int indexValue = cursor.getColumnIndexOrThrow(InfographicContract.WordlBankDataEntry.COLUMN_VALUE);
            int indexDate = cursor.getColumnIndexOrThrow(InfographicContract.WordlBankDataEntry.COLUMN_DATE);
            while (cursor.moveToNext()) {
                HashMap<String, String> hashMapData = new HashMap<>();
                hashMapData.put(VALUE_KEY, cursor.getString(indexValue));
                hashMapData.put(DATE_KEY, cursor.getString(indexDate));
                hashMapData.put(INDICATOR_KEY, dbIndicator);

                listDataModel.add(new DataModel(hashMapData));
            }
        }

        return listDataModel;
    }

    public static void insertData(HashMap<String, ArrayList<DataModel>> mapData, Context context) {
        for(Map.Entry<String, ArrayList<DataModel>> entry : mapData.entrySet())  {
            deleteOldData(entry.getKey(), context);
            insertSingleIndicatorData(entry.getKey(), entry.getValue(), context);
        }
    }

    private static void insertSingleIndicatorData(String dbIndicator, ArrayList<DataModel> listData, Context context) {
        for(DataModel dataModel : listData) {
            ContentValues values = new ContentValues();
            values.put(InfographicContract.WordlBankDataEntry.COLUMN_INDICATOR, dbIndicator);
            values.put(InfographicContract.WordlBankDataEntry.COLUMN_DATE, dataModel.getDate());
            values.put(InfographicContract.WordlBankDataEntry.COLUMN_VALUE, dataModel.getValue());

            context.getContentResolver().insert(
                    InfographicContract.WordlBankDataEntry.CONTENT_URI,
                    values
            );
        }
    }

    private static void deleteOldData(String dbIndicator, Context context) {
        String selectionClause = InfographicContract.WordlBankDataEntry.COLUMN_INDICATOR + " = ?";

        context.getContentResolver().delete(
                InfographicContract.WordlBankDataEntry.CONTENT_URI,
                selectionClause,
                new String[] {dbIndicator}
        );
    }
}
