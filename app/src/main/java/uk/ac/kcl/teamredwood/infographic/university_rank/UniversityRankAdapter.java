package uk.ac.kcl.teamredwood.infographic.university_rank;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import uk.ac.kcl.teamredwood.infographic.R;

public class UniversityRankAdapter extends RecyclerView.Adapter<UniversityRankAdapter.ViewHolder>  {

    private ArrayList<String[]> qsRankData;

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView tvPosition, tvUniversity, tvRank;

        public ViewHolder(View v) {
            super(v);
            tvPosition = (TextView) v.findViewById(R.id.item_position_rv);
            tvUniversity = (TextView) v.findViewById(R.id.university_name_rv);
            tvRank = (TextView) v.findViewById(R.id.university_rank_rv);
        }
    }

    public UniversityRankAdapter(ArrayList<String[]> qsRankData) {
        this.qsRankData = qsRankData;
    }

    @Override
    public UniversityRankAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                             int viewType) {

        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_university, parent, false);
        ViewHolder vh = new ViewHolder(view);

        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.tvPosition.setText("" + (position + 1));
        holder.tvUniversity.setText(qsRankData.get(position)[0]);
        holder.tvRank.setText(qsRankData.get(position)[1]);
    }

    @Override
    public int getItemCount() {
        if (qsRankData == null) { return 0; }
        return qsRankData.size();
    }

    public void updateList(ArrayList<String[]> data) {
        qsRankData = data;
        notifyDataSetChanged();
    }
}
