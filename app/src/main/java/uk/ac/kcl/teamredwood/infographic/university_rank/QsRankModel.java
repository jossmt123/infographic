package uk.ac.kcl.teamredwood.infographic.university_rank;

import android.content.Context;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class QsRankModel {

    private static final String LOG_TAG = QsRankModel.class.getSimpleName();

    private Context context;

    private ArrayList<UniversityModel> listGlobalRank = new ArrayList<>();
    private ArrayList<UniversityModel> listCitiesRank = new ArrayList<>();

    public QsRankModel(Context context) {
        this.context = context;
        initUniversityRankData();
    }

    public ArrayList<UniversityModel> getUniversityRankData() { return listGlobalRank; }

    public ArrayList<UniversityModel> getCitiesRankData() {
        return listGlobalRank;
    }


    public ArrayList<String[]> getArrayGlobalRankModel() {
        ArrayList<String[]> listRankModel = new ArrayList<>();
        for(int i = 0; i < listGlobalRank.size(); ++i) {
            UniversityModel model = listGlobalRank.get(i);
            listRankModel.add(new String[] {model.getName(), model.getRank()});
        }
        return listRankModel;
    }

    public ArrayList<String[]> getArrayCitiesRankModel() {
        ArrayList<String[]> listRankModel = new ArrayList<>();
        for(int i = 0; i < listCitiesRank.size(); ++i) {
            UniversityModel model = listCitiesRank.get(i);
            listRankModel.add(new String[] {model.getName(), model.getRank()});
        }
        return listRankModel;
    }

    public void initUniversityRankData() {
        listGlobalRank = readCsv("qsrankings14.csv");
        listCitiesRank = readCsv("qsrankcities.csv");
    }

    public ArrayList<UniversityModel> readCsv(String path) {
        ArrayList<UniversityModel> output = new ArrayList<>();
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(
                    new InputStreamReader(context.getAssets().open(path)));
            String line;
            while ((line = reader.readLine()) != null) {
                String[] arrLine = line.split(",");
                output.add(new UniversityModel(arrLine[0], arrLine[1], arrLine[2]));
            }
        } catch (IOException e) {
            Log.e(LOG_TAG, e.getMessage());
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    Log.e(LOG_TAG, e.getMessage());
                }
            }
        }
        return output;
    }

    public ArrayList<String[]> getArrayLocalRankModel(String country) {
        ArrayList<String[]> listRankModel = new ArrayList<>();
        for(int i = 0; i < listGlobalRank.size(); ++i) {
            UniversityModel model = listGlobalRank.get(i);
            if(model.getCountry().equals(country)) {
                listRankModel.add(new String[]{model.getName(), model.getRank()});
            }
        }
        return listRankModel;
    }
}
