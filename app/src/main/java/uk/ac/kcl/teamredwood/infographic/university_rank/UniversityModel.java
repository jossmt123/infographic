package uk.ac.kcl.teamredwood.infographic.university_rank;

public class UniversityModel {

    private String rank, name, country;

    public UniversityModel(String rank, String name, String country) {
        this.rank = rank;
        this.name = name;
        this.country = country;
    }

    public String getRank() {
        return rank;
    }

    public String getName() {
        return name;
    }

    public String getCountry() {
        return country;
    }

    public String[] getArrayModel() {
        return new String[] {rank, name};
    }
}
