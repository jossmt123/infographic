package uk.ac.kcl.teamredwood.infographic.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import uk.ac.kcl.teamredwood.infographic.database.InfographicContract.WordlBankDataEntry;

/**
 * Manages a local database for the World Bank API data.
 */
public class InfographicDbHelper extends SQLiteOpenHelper {

    // The database version must be incremented when the database schema is changed
    private static final int DATABASE_VERSION = 1;

    static final String DATABASE_NAME = "infographic.db";

    public InfographicDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        // Create a table to collect data from the World Bank API
        final String SQL_CREATE_WORLD_BANK_TABLE = "CREATE TABLE " + WordlBankDataEntry.TABLE_NAME + " (" +
                WordlBankDataEntry._ID + " INTEGER PRIMARY KEY, " +
                WordlBankDataEntry.COLUMN_INDICATOR + " TEXT NOT NULL, " +
                WordlBankDataEntry.COLUMN_DATE + " TEXT NOT NULL, " +
                WordlBankDataEntry.COLUMN_VALUE + " TEXT NOT NULL " +
                " );";

        db.execSQL(SQL_CREATE_WORLD_BANK_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {}
}
