package uk.ac.kcl.teamredwood.infographic;

import android.app.DialogFragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Spinner;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;


import org.honorato.multistatetogglebutton.MultiStateToggleButton;
import org.honorato.multistatetogglebutton.ToggleButton;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.text.DecimalFormat;

import uk.ac.kcl.teamredwood.infographic.model.DataModel;
import uk.ac.kcl.teamredwood.infographic.model.FetchDataTask;
import uk.ac.kcl.teamredwood.infographic.university_rank.QsRankModel;
import uk.ac.kcl.teamredwood.infographic.university_rank.UniversityRankAdapter;

public class MainActivity extends AppCompatActivity implements FetchDataTask.CallbackDataTask {

    private ArrayList<String> values;
    private ArrayList<ArrayList<DataModel>> countries;
    private String wantedVal;
    public final static String LOG_TAG = MainActivity.class.getSimpleName();

    /**
     * Map that stores lists of data models.
     *
     * Key - String in the form of "country/indicator", e.g. FRA/14.1_AGR.ENERGY.INTENSITY
     * Value - the list of data models for a specific indicator
     */
    private HashMap<String, ArrayList<DataModel>> mapDataModelArrays = new HashMap<>();

    // The list of all active tasks
    private ArrayList<AsyncTask> listActiveTasks = new ArrayList<>();

    private UniversityRankAdapter adapter;

    private ArrayList<String[]> listGlobalRank;
    private QsRankModel qsRankModel;
    public String selectedCountry = "United Kingdom";
    private int selectedCountryValue = 0;

    ImageView image;
    TextView tvPercentage;
    public int percentage;
    private DialogFragment dialog;
    ProgressDialog progressDialog;
    private MultiStateToggleButton multiButton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);

        image = (ImageView)findViewById(R.id.imageView);
        tvPercentage = (TextView)findViewById(R.id.percentage);
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(this);
            progressDialog.setMessage(this.getString(R.string.progress_dialog));
            progressDialog.show();
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.setCancelable(false);
        }
        createDataModels();
    }

    /**
     * Called after FetchDataTask finishes fetching data.
     *
     * Creates a list of data models, inserts that list to a map,
     * and checks whether all tasks are finished.
     *
     * @param jsonResponse
     * @param mapIndicator
     */
    @Override
    public void onTaskFinished(String jsonResponse, String mapIndicator) {
        if (jsonResponse == null) {
            Log.e(LOG_TAG, "Error retrieving data, probably network connectivity issue");
            return;
        }
        try {
            ArrayList<DataModel> listDataModel = DataModel.parseJSON(jsonResponse, mapIndicator);
            mapDataModelArrays.put(mapIndicator, listDataModel);
        } catch (JSONException e) {
            Log.e(LOG_TAG, e.getMessage());
        }
        if (allTasksFinished()) {
            onAllTasksFinished();
        }
    }

    /**
     * Invoked after all tasks computation finishes.
     * Used to create/populate diagrams and insert data into the database.
     */
    @Override
    public void onAllTasksFinished() {
        DataModel.insertData(mapDataModelArrays, this);
        if (progressDialog.isShowing()) {
            progressDialog.setMessage("Initializing UI.");
        }
        initializeUI();
        if (progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }

    public void get2012(ArrayList<DataModel> datamodel){
        for(int i=0; i<datamodel.size(); i++){
            DataModel model = datamodel.get(i);
            String date = model.getDate();
            if(date.equals("2012")){
                wantedVal = model.getValue();
            }
        }
        Log.d(LOG_TAG, wantedVal);
        values.add(wantedVal);
    }

    private void initializeUI() {
        values = new ArrayList<>();
        countries = new ArrayList<>();

        ArrayList<DataModel> gbpIndicator = mapDataModelArrays.get("GBR/SL.TLF.PART.ZS");
        ArrayList<DataModel> ausIndicator = mapDataModelArrays.get("AUS/SL.TLF.PART.ZS");
        ArrayList<DataModel> itaIndicator = mapDataModelArrays.get("ITA/SL.TLF.PART.ZS");
        ArrayList<DataModel> rusIndicator = mapDataModelArrays.get("RUS/SL.TLF.PART.ZS");
        ArrayList<DataModel> usaIndicator = mapDataModelArrays.get("USA/SL.TLF.PART.ZS");
        ArrayList<DataModel> espIndicator = mapDataModelArrays.get("ESP/SL.TLF.PART.ZS");
        ArrayList<DataModel> fraIndicator = mapDataModelArrays.get("FRA/SL.TLF.PART.ZS");
        ArrayList<DataModel> jpnIndicator = mapDataModelArrays.get("JPN/SL.TLF.PART.ZS");
        ArrayList<DataModel> gerIndicator = mapDataModelArrays.get("DEU/SL.TLF.PART.ZS");
        ArrayList<DataModel> sweIndicator = mapDataModelArrays.get("SWE/SL.TLF.PART.ZS");
        countries.add(gbpIndicator);countries.add(ausIndicator); countries.add(itaIndicator);
        countries.add(rusIndicator);countries.add(usaIndicator);countries.add(espIndicator);
        countries.add(fraIndicator);countries.add(jpnIndicator);countries.add(gerIndicator);
        countries.add(sweIndicator);

        for(int i=0; i<countries.size(); i++) {
            get2012(countries.get(i));
            Log.d(LOG_TAG, values.get(i));
        }
        qsRankModel = new QsRankModel(this);
        listGlobalRank = qsRankModel.getArrayGlobalRankModel();
        initializeRecyclerView();

        multiButton = (MultiStateToggleButton) this.findViewById(R.id.mstb_multi_id);
        boolean[] arrBool = {false, true, false};
        multiButton.setStates(arrBool);
        multiButton.setOnValueChangedListener(new ToggleButton.OnValueChangedListener() {
            @Override
            public void onValueChanged(int position) {
                ArrayList<String[]> list = new ArrayList<>();
                switch (position) {
                    case 0:
                        list = qsRankModel.getArrayCitiesRankModel();
                        break;
                    case 1:
                        list = qsRankModel.getArrayLocalRankModel(selectedCountry);
                        break;
                    case 2:
                        list = listGlobalRank;
                }
                adapter.updateList(list);
            }
        });
        updateEmploymentDiagram(selectedCountryValue);
        updateEmploymentStatistics(selectedCountry);
    }

    /**
     * Checks whether all tasks have finished execution.
     *
     * @return false if any task is still running
     */
    public boolean allTasksFinished() {
        for (int i = 0; i < listActiveTasks.size(); ++i) {
            if(listActiveTasks.get(i).getStatus() == AsyncTask.Status.FINISHED) {
                listActiveTasks.remove(i);
            }
        }
        // <= 1 Because when getStatus is called on a task that has invoked this method
        // then its status is still "running"
        return listActiveTasks.size() <= 1;
    }

    /**
     * Retrieves data from the World Bank Api if there is network connection.
     * Otherwise retrieves data that is stored locally.
     */
    public void createDataModels() {
        if(isNetworkAvailable(this)) {
            // pulling data for Part-time employment % of total
            String [] countryCodes = {"GBR", "AUS", "ITA", "RUS", "USA", "ESP", "FRA", "JPN", "DEU", "SWE"};
            String [] indicatordIds = {"SL.TLF.PART.ZS", "SE.XPD.TOTL.GB.ZS", "SL.TLF.TERT.ZS"};
            for(String countryCode : countryCodes){
                for(String indicator : indicatordIds){
                    createDataModel(countryCode, indicator);
                }
            }

        } else {
            Toast.makeText(this, getString(R.string.network_connection), Toast.LENGTH_LONG).show();

            ArrayList<DataModel> firsData = DataModel.retrieveData("GBR/SL.TLF.PART.ZS", this);

            if (!firsData.isEmpty()) {

                mapDataModelArrays.put("GBR/SL.TLF.PART.ZS",
                        firsData);
                mapDataModelArrays.put("AUS/SL.TLF.PART.ZS",
                        DataModel.retrieveData("AUS/SL.TLF.PART.ZS", this));
                mapDataModelArrays.put("ITA/SL.TLF.PART.ZS",
                        DataModel.retrieveData("ITA/SL.TLF.PART.ZS", this));
                mapDataModelArrays.put("RUS/SL.TLF.PART.ZS",
                        DataModel.retrieveData("RUS/SL.TLF.PART.ZS", this));
                mapDataModelArrays.put("USA/SL.TLF.PART.ZS",
                        DataModel.retrieveData("USA/SL.TLF.PART.ZS", this));
                mapDataModelArrays.put("ESP/SL.TLF.PART.ZS",
                        DataModel.retrieveData("ESP/SL.TLF.PART.ZS", this));
                mapDataModelArrays.put("FRA/SL.TLF.PART.ZS",
                        DataModel.retrieveData("FRA/SL.TLF.PART.ZS", this));
                mapDataModelArrays.put("JPN/SL.TLF.PART.ZS",
                        DataModel.retrieveData("JPN/SL.TLF.PART.ZS", this));
                mapDataModelArrays.put("DEU/SL.TLF.PART.ZS",
                        DataModel.retrieveData("DEU/SL.TLF.PART.ZS", this));
                mapDataModelArrays.put("SWE/SL.TLF.PART.ZS",
                        DataModel.retrieveData("SWE/SL.TLF.PART.ZS", this));

                if (progressDialog.isShowing()) {
                    progressDialog.setMessage("Initializing UI.");
                }
                    initializeUI();
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
            } else {
                if (progressDialog.isShowing()) {
                    progressDialog.setMessage(getString(R.string.first_time_network_error));
                }
            }
        }
    }

    /**
     * Initializes a new task and adds it to the list of active tasks.
     *
     * @param country country name, an empty string if not required
     * @param indicator indicator name from the World Bank data
     */
    public void createDataModel(String country, String indicator) {
        AsyncTask newTask = new FetchDataTask(this).execute(country, indicator);
        listActiveTasks.add(newTask);
    }

    /**
     * @return true if there is network connection
     */
    static public boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    private void initializeRecyclerView() {
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.universities_recycler_view);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);

        ArrayList<String[]> qsRankData = new ArrayList<>();
        qsRankData.addAll(qsRankModel.getArrayLocalRankModel(selectedCountry));

        adapter = new UniversityRankAdapter(qsRankData);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_country_btn:
                dialog = new CountryDialogFragment();
                dialog.show(getFragmentManager(), CountryDialogFragment.class.getSimpleName());
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void countrySelected(View view) {
        if(dialog != null && dialog.getDialog() != null) {
            selectedCountry = getSelectedCountry(view);
            selectedCountryValue = getSelectedCountryVal(view);
            if(multiButton.getValue() == 1) {
                adapter.updateList(qsRankModel.getArrayLocalRankModel(selectedCountry));
            }

            ((TextView) dialog.getDialog().findViewById(R.id.tv_country)).setText(selectedCountry);
            ((android.support.v7.view.menu.ActionMenuItemView)
                    findViewById(R.id.menu_country_btn)).setTitle(" " + selectedCountry);

            updateEmploymentDiagram(selectedCountryValue);
            updateEmploymentStatistics(selectedCountry);
        }
    }

    private void updateEmploymentDiagram(int x) {

        float f = Float.parseFloat(values.get(x));
        int percentage = Math.round(f);
        Log.d(LOG_TAG, percentage+"");
        tvPercentage.setText(percentage + "%");

        if(percentage<=10){
            image.setImageResource(R.drawable.employment10);
        }else if(percentage>10 && percentage<=15){
            image.setImageResource(R.drawable.employment15);
        }else if(percentage>15 && percentage<=20){
            image.setImageResource(R.drawable.employment20);
        }else if(percentage>20 && percentage<=25) {
            image.setImageResource(R.drawable.employment25);
        }else if(percentage>25 && percentage<=30) {
            image.setImageResource(R.drawable.employment30);
        }else if(percentage>30 && percentage<=35) {
            image.setImageResource(R.drawable.employment35);
        }else if(percentage>35) {
            image.setImageResource(R.drawable.employment40);
        }
    }

    private String getSelectedCountry(View view) {
        switch(view.getId()) {
            case R.id.btnAustralia:
                return "Australia";
            case R.id.btnFrance:
                return "France";
            case R.id.btnGermany:
                return "Germany";
            case R.id.btnItaly:
                return "Italy";
            case R.id.btnJapan:
                return "Japan";
            case R.id.btnRussia:
                return "Russia";
            case R.id.btnSpain:
                return "Spain";
            case R.id.btnSweden:
                return "Sweden";
            case R.id.btnUnitedKingdom:
                return "United Kingdom";
            case R.id.btnUnitedStates:
                return "United States";
            default:
                return "United Kingdom";
        }
    }

    private int getSelectedCountryVal(View view) {
        switch(view.getId()) {
            case R.id.btnAustralia:
                return 1;
            case R.id.btnFrance:
                return 6;
            case R.id.btnGermany:
                return 8;
            case R.id.btnItaly:
                return 2;
            case R.id.btnJapan:
                return 7;
            case R.id.btnRussia:
                return 3;
            case R.id.btnSpain:
                return 5;
            case R.id.btnSweden:
                return 9;
            case R.id.btnUnitedKingdom:
                return 0;
            case R.id.btnUnitedStates:
                return 4;
            default:
                return 0;
        }
    }

    private void updateEmploymentStatistics(String country){

        String indicatorId = "";
        String govSpendingId = "";
        String labTertId = "";
        TextView cTv = (TextView)findViewById(R.id.countryText);
        //map flag button to indicators
        if(country.equals("Australia")){
            cTv.setText("Country: Australia");
            indicatorId = "AUS/SL.TLF.PART.ZS";
            govSpendingId = "AUS/SE.XPD.TOTL.GB.ZS";
            labTertId = "AUS/SL.TLF.TERT.ZS";
        }else if(country.equals("France")){
            cTv.setText("Country: France");
            indicatorId = "FRA/SL.TLF.PART.ZS";
            govSpendingId = "FRA/SE.XPD.TOTL.GB.ZS";
            labTertId = "FRA/SL.TLF.TERT.ZS";
        }else if(country.equals("Germany")){
            cTv.setText("Country: Germany");
            indicatorId = "DEU/SL.TLF.PART.ZS";
            govSpendingId = "DEU/SE.XPD.TOTL.GB.ZS";
            labTertId = "DEU/SL.TLF.TERT.ZS";
        }else if(country.equals("Italy")){
            cTv.setText("Country: Italy");
            indicatorId = "ITA/SL.TLF.PART.ZS";
            govSpendingId = "ITA/SE.XPD.TOTL.GB.ZS";
            labTertId = "ITA/SL.TLF.TERT.ZS";
        }else if(country.equals("Japan")){
            cTv.setText("Country: Japan");
            indicatorId = "JPN/SL.TLF.PART.ZS";
            govSpendingId = "JPN/SE.XPD.TOTL.GB.ZS";
            labTertId = "JPN/SL.TLF.TERT.ZS";
        }else if(country.equals("Russia")){
            cTv.setText("Country: Russia");
            indicatorId = "RUS/SL.TLF.PART.ZS";
            govSpendingId = "RUS/SE.XPD.TOTL.GB.ZS";
            labTertId = "RUS/SL.TLF.TERT.ZS";
        }else if(country.equals("Spain")){
            cTv.setText("Country: Spain");
            indicatorId = "ESP/SL.TLF.PART.ZS";
            govSpendingId = "ESP/SE.XPD.TOTL.GB.ZS";
            labTertId = "ESP/SL.TLF.TERT.ZS";
        }else if(country.equals("Sweden")){
            cTv.setText("Country: Sweden");
            indicatorId = "SWE/SL.TLF.PART.ZS";
            govSpendingId = "SWE/SE.XPD.TOTL.GB.ZS";
            labTertId = "SWE/SL.TLF.TERT.ZS";
        }else if(country.equals("United Kingdom")){
            cTv.setText("Country: United Kingdom");
            indicatorId = "GBR/SL.TLF.PART.ZS";
            govSpendingId = "GBR/SE.XPD.TOTL.GB.ZS";
            labTertId = "GBR/SL.TLF.TERT.ZS";
        }else if(country.equals("United States")){
            cTv.setText("United States");
            indicatorId = "USA/SL.TLF.PART.ZS";
            govSpendingId = "USA/SE.XPD.TOTL.GB.ZS";
            labTertId = "USA/SL.TLF.TERT.ZS";
        }

        //check to ensure indicator selected.
        if(indicatorId.length() == 0){
            return;
        }

        //============

        final List<DataModel> dmListForIndicatorId= DataModel.retrieveData(indicatorId, getApplicationContext());
        final List<DataModel> dmListForGovSpendingId = DataModel.retrieveData(govSpendingId, getApplicationContext());
        final List<DataModel> dmListForLabTertEduId = DataModel.retrieveData(labTertId, getApplicationContext());

        //add data to spinner
        ArrayList<String> l = new ArrayList<String>();
        for(DataModel dm : dmListForIndicatorId){
            if(dm.getDate() != null && dm.getDate().length() == 4){
                l.add(dm.getDate());
            }
        }

        Spinner s = (Spinner) findViewById(R.id.countrySpinner);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, l.toArray(new String[l.size()]));
        s.setAdapter(adapter);
        s.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            public void onItemSelected(AdapterView<?> parent, View view,
                                       int pos, long id) {

                //show values for part-time employment
                String year = parent.getItemAtPosition(pos).toString();

                boolean partTimeDataIsAvailable = false;
                TextView tv1 = (TextView)findViewById(R.id.percentage);
                for(DataModel dm : dmListForIndicatorId){
                    if(dm.getDate().equals(year)){
                        String val = dm.getValue();
                        double dVal = Double.parseDouble(val);
                        DecimalFormat df = new DecimalFormat("#.###");
                        //set percentage
                        tv1.setText(df.format(dVal)+"%");
                        //set image
                        ImageView iv = (ImageView)findViewById(R.id.imageView);
                        if(dVal <= 5){
                            iv.setImageResource(R.drawable.employment5);
                        }else if(dVal <= 10){
                            iv.setImageResource(R.drawable.employment10);
                        }else if(dVal <= 15){
                            iv.setImageResource(R.drawable.employment15);
                        }else if(dVal <= 20){
                            iv.setImageResource(R.drawable.employment20);
                        }else if(dVal <= 25){
                            iv.setImageResource(R.drawable.employment25);
                        }else if(dVal <= 30){
                            iv.setImageResource(R.drawable.employment30);
                        }else if(dVal <= 35){
                            iv.setImageResource(R.drawable.employment35);
                        }else{
                            iv.setImageResource(R.drawable.employment40);
                        }
                        partTimeDataIsAvailable = true;
                    }
                }
                if(!partTimeDataIsAvailable){
                    tv1.setText("N/A");
                }

                //show values for gov spending
                boolean govSpendingIsAvailable = false;
                TextView tv2 = (TextView)findViewById(R.id.textView4);
                for(DataModel dm : dmListForGovSpendingId){
                    if(dm.getDate().equals(year)){
                        String val = dm.getValue();
                        double dVal = Double.parseDouble(val);
                        DecimalFormat df = new DecimalFormat("#.##");
                        //set percentage
                        tv2.setText(df.format(dVal)+"%");
                        govSpendingIsAvailable = true;
                    }
                }

                if(!govSpendingIsAvailable){
                    tv2.setText("N/A");
                }

                //show values for gov spending
                boolean labTertEduIsAvailable = false;
                TextView tv3 = (TextView)findViewById(R.id.edPercentage);
                for(DataModel dm : dmListForLabTertEduId){
                    if(dm.getDate().equals(year)){
                        String val = dm.getValue();
                        double dVal = Double.parseDouble(val);
                        DecimalFormat df = new DecimalFormat("#.##");
                        //set percentage
                        tv3.setText(df.format(dVal)+"%");
                        labTertEduIsAvailable = true;
                    }
                }

                if(!labTertEduIsAvailable){
                    tv3.setText("N/A");
                }

            }

            public void onNothingSelected(AdapterView<?> parent) {

            }

        });
    }
}